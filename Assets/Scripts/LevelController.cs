﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    [SerializeField] private GameObject winLabel;
    [SerializeField] private GameObject loseLabel;
    [SerializeField] private float timeBeforeNextLevel = 5f;

    private SceneLoader _sceneLoader;
    private AudioSource _audioSource;

    // Start is called before the first frame update
    void Start()
    {
        winLabel.SetActive(false);
        loseLabel.SetActive(false);

        _sceneLoader = FindObjectOfType<SceneLoader>();
        _audioSource = GetComponent<AudioSource>();
    }

    private IEnumerator LevelWin()
    {
        winLabel.SetActive(true);
        _audioSource.Play();

        yield return new WaitForSeconds(timeBeforeNextLevel);
        _sceneLoader.LoadNextScene();
    }

    public void LevelLose()
    {
        loseLabel.SetActive(true);

        // We stop time
        Time.timeScale = 0.05f;
    }

    public void AddOneAttacker()
    {
        AttackersCount++;
    }

    public void RemoveOneAttacker()
    {
        AttackersCount--;
        if (TimerFinished && AttackersCount <= 0)
        {
            StartCoroutine(LevelWin());
        }
    }

    public bool TimerFinished { get; set; }

    private int AttackersCount { get; set; }
}