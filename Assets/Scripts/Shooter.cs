﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    [SerializeField] private Projectile projectile;
    [SerializeField] private GameObject gun;

    private readonly List<AttackerSpawner> _laneSpawners = new List<AttackerSpawner>();
    private Animator _animator;
    private GameObject _projectileParent;

    private const string PROJECTILE_PARENT_NAME = "Projectiles";

    private void Start()
    {
        _animator = GetComponent<Animator>();
        CreateProjectileParent();
        SetLaneSpawners();
    }

    private void CreateProjectileParent()
    {
        _projectileParent = GameObject.Find(PROJECTILE_PARENT_NAME);
        if (!_projectileParent) _projectileParent = new GameObject(PROJECTILE_PARENT_NAME);
    }

    private void SetLaneSpawners()
    {
        var yPosition = transform.position.y;
        var allSpawners = FindObjectsOfType<AttackerSpawner>();

        foreach (var spawner in allSpawners)
        {
            var isCloseEnough = Mathf.Abs(spawner.transform.position.y - yPosition) <= Mathf.Epsilon;
            if (isCloseEnough)
            {
                _laneSpawners.Add(spawner);
            }
        }
    }

    private void Update()
    {
        _animator.SetBool("IsAttacking", IsAttackerInLane());
    }

    private bool IsAttackerInLane()
    {
        foreach (var laneSpawner in _laneSpawners)
        {
            if (laneSpawner.transform.childCount > 0) return true;
        }

        return false;
    }


    public void Fire()
    {
        var newProjectile = Instantiate(projectile, gun.transform.position, Quaternion.identity);
        newProjectile.transform.parent = _projectileParent.transform;
    }
}