﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour
{
    [Tooltip("Our level Time in Seconds")]
    [SerializeField] private float levelTime = 10f;

    private Slider _slider;
    private LevelController _levelController;
    private bool _triggeredLevelFinished;

    private void Start()
    {
        _slider = GetComponent<Slider>();
        _levelController = FindObjectOfType<LevelController>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if(_triggeredLevelFinished) return;
        
        var timePercentage = Time.timeSinceLevelLoad / levelTime;
        _slider.value = timePercentage;
        
        if (timePercentage >=1)
        {
            _levelController.TimerFinished = true;
            _triggeredLevelFinished = true;
        }

    }
}
