﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attacker : MonoBehaviour
{
    [Range(0f, 5f)] private float _currentSpeed = 1f;

    private GameObject _currentTarget;
    private Animator _animator;
    private LevelController _levelController;

    private void Awake()
    {
        _levelController = FindObjectOfType<LevelController>();
        _levelController.AddOneAttacker();
    }


    // Start is called before the first frame update
    private void Start()
    {
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void Update()
    {
        transform.Translate(Vector2.left * (Time.deltaTime * _currentSpeed));
        UpdateAnimationState();
    }

    private void OnDestroy()
    {
        _levelController.RemoveOneAttacker();
    }

    private void UpdateAnimationState()
    {
        if (!_currentTarget) _animator.SetBool("IsAttacking", false);
    }

    public void SetMovementSpeed(float speed)
    {
        _currentSpeed = speed;
    }

    public void Attack(GameObject target)
    {
        _animator.SetBool("IsAttacking", true);
        _currentTarget = target;
    }

    public void StrikeCurrentTarget(int damage)
    {
        if (!_currentTarget) return;
        var health = _currentTarget.GetComponent<Health>();
        if (health)
        {
            health.DealDamage(damage);
        }
    }
}