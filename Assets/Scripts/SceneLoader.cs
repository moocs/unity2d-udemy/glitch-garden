﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] private float startScreenLoadingDelay = 2f;

    private int _currentSceneIndex;

    void Start()
    {
        _currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        if (_currentSceneIndex == 0)
        {
            StartCoroutine(NextSceneWithDelay());
        }
    }

    public void LoadNextScene()
    {
        SceneManager.LoadScene(_currentSceneIndex + 1);
    }

    private IEnumerator NextSceneWithDelay()
    {
        yield return new WaitForSeconds(startScreenLoadingDelay);
        LoadNextScene();
    }

    public static void LoadLoseScreen()
    {
        SceneManager.LoadScene("LoseScreen");
    }

    public void LoadStartScreen()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("StartScreen");
    }

    public void LoadOptionScreen()
    {
        SceneManager.LoadScene("OptionScreen");
    }

    public void ReloadCurrentScreen()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(_currentSceneIndex);
    }

    public static void QuitGame()
    {
        Application.Quit();
    }
}