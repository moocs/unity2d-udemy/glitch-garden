﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenderButtons : MonoBehaviour
{
    [SerializeField] private Defender defenderPrefab;
    
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<SpriteRenderer>().color = Color.grey;
    }

    private void OnMouseDown()
    {
        var buttons = FindObjectsOfType<DefenderButtons>();
        foreach (var button in buttons)
        {
            button.GetComponent<SpriteRenderer>().color = Color.grey;
        }

        GetComponent<SpriteRenderer>().color = Color.white;
        FindObjectOfType<DefenderSpawner>().Defender = defenderPrefab;
    }
}