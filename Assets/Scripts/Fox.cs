﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fox : MonoBehaviour
{

    private Animator _animator;

    private void Start()
    {
        _animator = GetComponent<Animator>();
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        var otherObject = other.gameObject;
        if (!otherObject.GetComponent<Defender>()) return;
        if (otherObject.GetComponent<GraveStone>()) _animator.SetTrigger("JumpTrigger");
        else GetComponent<Attacker>().Attack(otherObject);
    }
}
