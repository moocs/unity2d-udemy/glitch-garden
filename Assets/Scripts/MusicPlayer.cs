﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    private AudioSource _audioSource;

    private void Awake()
    {
        SetUpSingleton();
    }

    private void SetUpSingleton()
    {
        // Implementing Singleton
        if (FindObjectsOfType(GetType()).Length > 1)
        {
            // If there are already existing gameStatus, destroy yourself as you're not the first one
            GameObject o;
            (o = gameObject).SetActive(false);
            Destroy(o);
        }
        else
        {
            // Otherwise, you're the first : you can exist and you should not get destroyed at the end of the Scene
            DontDestroyOnLoad(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        _audioSource.volume = PlayerPrefsController.GetMasterVolume();
    }

    public void SetVolume(float volume)
    {
        _audioSource.volume = volume;
    }

    public void SetVolume()
    {
        _audioSource.volume = PlayerPrefsController.GetMasterVolume();
    }
}