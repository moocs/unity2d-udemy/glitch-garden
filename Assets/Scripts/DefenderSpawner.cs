﻿using System;
using UnityEngine;

public class DefenderSpawner : MonoBehaviour
{
    private Camera _mainCamera;
    private StarDisplay _starDisplay;
    private GameObject _defenderParent;
    private const string DEFENDER_PARENT_NAME = "Defenders";

    public Defender Defender { get; set; }

    private void Start()
    {
        _mainCamera = Camera.main;
        _starDisplay = FindObjectOfType<StarDisplay>();
        CreateDefenderParent();
        
        Debug.Assert(_mainCamera && _starDisplay);
    }

    private void OnMouseDown()
    {
        var spawnPosition = GetSpawnPosition();
        var gridPosition = SnapToGrid(spawnPosition);

        SpawnDefender(gridPosition);
    }

    private void CreateDefenderParent()
    {
        _defenderParent = GameObject.Find(DEFENDER_PARENT_NAME);
        if (!_defenderParent)
        {
            _defenderParent = new GameObject(DEFENDER_PARENT_NAME);
        }
    }


    private Vector2 GetSpawnPosition()
    {
        return _mainCamera.ScreenToWorldPoint(Input.mousePosition);
    }

    private static Vector2 SnapToGrid(Vector2 worldPos)
    {
        return new Vector2(Mathf.RoundToInt(worldPos.x), Mathf.RoundToInt(worldPos.y));
    }

    private void SpawnDefender(Vector2 worldPos)
    {
        if (!Defender) return;
        if (!_starDisplay.SpendStars(Defender.StarCost)) return;

        var defender = Instantiate(Defender, worldPos, Quaternion.identity);
        defender.transform.parent = _defenderParent.transform;
    }
}