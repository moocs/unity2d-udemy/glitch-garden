﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 5f;
    [SerializeField] private int damage = 100;


    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.right * (Time.deltaTime * moveSpeed));
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var healthManager = other.gameObject.GetComponent<Health>();
        var attacker = other.GetComponent<Attacker>();

        if (!healthManager || !attacker) return;
        
        healthManager.DealDamage(damage);
        Destroy(gameObject);
    }
}