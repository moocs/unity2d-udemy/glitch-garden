﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackerSpawner : MonoBehaviour
{
    [SerializeField] private Attacker[] attackersPrefab;
    [SerializeField] private float minSPawnDelay = 1f;
    [SerializeField] private float maxSPawnDelay = 5f;
    [SerializeField] private float initTimeToWaitSeconds = 2f;

    private LevelController _levelController;

    // Start is called before the first frame update
    private IEnumerator Start()
    {
        _levelController = FindObjectOfType<LevelController>();
        var difficulty = PlayerPrefsController.GetDifficulty();
        
        // First waiting time before game start
        yield return new WaitForSeconds(initTimeToWaitSeconds);
        
        while (!_levelController.TimerFinished)
        {
            SpawnRandomAttacker();

            var spawnDelay = Random.Range(minSPawnDelay, maxSPawnDelay) / difficulty;
            yield return new WaitForSeconds(spawnDelay);
        }
    }

    private void SpawnRandomAttacker()
    {
        var attackerToSpawn = attackersPrefab[Random.Range(0, attackersPrefab.Length)];
        SpawnAttacker(attackerToSpawn);
    }

    private void SpawnAttacker(Attacker attackerPrefab)
    {
        var attacker = Instantiate(attackerPrefab, transform.position, Quaternion.identity);
        attacker.transform.parent = transform;
    }
}