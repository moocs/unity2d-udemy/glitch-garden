﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsController : MonoBehaviour
{
    [Header("Master Volume")] [SerializeField]
    private Slider volumeSlider;

    [SerializeField] private float defaultVolume = 0.8f;

    [Header("Difficulty")] [SerializeField]
    private Slider difficultySlider;

    [SerializeField] private int defaultDifficulty = 2;

    private MusicPlayer _musicPlayer;

    // Start is called before the first frame update
    void Start()
    {
        volumeSlider.value = PlayerPrefsController.GetMasterVolume();
        difficultySlider.value = PlayerPrefsController.GetDifficulty();
    }

    public void SetVolumeValue()
    {
        var volume = volumeSlider.value;
        PlayerPrefsController.SetMasterVolume(volume);

        if (!_musicPlayer) _musicPlayer = FindObjectOfType<MusicPlayer>();
        if (_musicPlayer)
        {
            _musicPlayer.SetVolume();
        }
        else
        {
            Debug.LogError("No MusicPlayer found... Did you start on Option Screen ?");
        }
    }

    public void SetDifficulty()
    {
        // The difficulty sets the spawn frequency
        var difficulty = (int) difficultySlider.value;
        PlayerPrefsController.SetDifficulty(difficulty);
    }
    
    public void SetDefaults()
    {
        volumeSlider.value = defaultVolume;
        difficultySlider.value = defaultDifficulty;
    }
}