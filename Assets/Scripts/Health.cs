﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private int maxHealth = 200;
    [SerializeField] private GameObject deathVfxPrefab;

    private int _currentHealth;
    private GameObject _vfxParent;
    private const string VFX_PARENT_NAME = "VFX";


    // Start is called before the first frame update
    void Start()
    {
        _currentHealth = maxHealth;
        CreateVfxParent();
        
    }
    
    private void CreateVfxParent()
    {
        _vfxParent = GameObject.Find(VFX_PARENT_NAME);
        if (!_vfxParent) _vfxParent = new GameObject(VFX_PARENT_NAME);
    }


    // Update is called once per frame
    void Update()
    {
    }

    public void DealDamage(int damage)
    {
        _currentHealth -= damage;
        if (_currentHealth <= 0)
        {
            TriggerDeathVfx();
            Destroy(gameObject);
        }
    }

    private void TriggerDeathVfx()
    {
        if (!deathVfxPrefab) return;

        var deathvfxObject = Instantiate(deathVfxPrefab, transform.position, Quaternion.identity);
        deathvfxObject.transform.parent = _vfxParent.transform;

        Destroy(deathvfxObject, 5f);
    }
}