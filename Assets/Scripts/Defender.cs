﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Defender : MonoBehaviour
{
    [SerializeField] private int starCost = 10;

    private StarDisplay _starDisplay;

    private void Start()
    {
        _starDisplay = FindObjectOfType<StarDisplay>();
    }

    public int StarCost => starCost;

    public void CollectStars(int starsToAdd)
    {
        _starDisplay.AddStars(starsToAdd);
    }
}