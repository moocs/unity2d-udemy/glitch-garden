﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OurBase : MonoBehaviour
{
    private HealthDisplay _healthDisplay;

    private void Start()
    {
        _healthDisplay = FindObjectOfType<HealthDisplay>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var otherObject = other.gameObject;
        if (!otherObject.GetComponent<Attacker>()) return;

        _healthDisplay.GetHit();
        Destroy(otherObject, 2f);
    }

}