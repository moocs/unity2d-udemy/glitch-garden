﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StarDisplay : MonoBehaviour
{
    [SerializeField] private int starCount = 100;
    private Text _starText;

    // Start is called before the first frame update
    void Start()
    {
        _starText = GetComponent<Text>();
        Display();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void AddStars(int starsToAdd)
    {
        StarCount += starsToAdd;
    }

    public bool SpendStars(int starsToRemove)
    {
        if (starsToRemove > starCount) return false;
        StarCount -= starsToRemove;
        return true;
    }

    private int StarCount
    {
        get => starCount;
        set
        {
            starCount = value;
            Display();
        }
    }

    private void Display()
    {
        _starText.text = starCount.ToString();
    }
}