﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthDisplay : MonoBehaviour
{
    private int _playerHealth;
    private Text _healthText;
    private LevelController _levelController;

    // Start is called before the first frame update
    void Start()
    {
        _levelController = FindObjectOfType<LevelController>();
        _healthText = GetComponent<Text>();

        _playerHealth = 6 - PlayerPrefsController.GetDifficulty();
        Display();
    }

    public void GetHit()
    {
        Health -= 1;
        if (Health <= 0) GameOver();
    }

    private void GameOver()
    {
        _levelController.LevelLose();
    }

    private int Health
    {
        get => _playerHealth;
        set
        {
            _playerHealth = value;
            Display();
        }
    }

    private void Display()
    {
        _healthText.text = _playerHealth.ToString();
    }
}